package services;


import dao.Database;
import entity.*;
import java.util.*;
import java.util.stream.Collectors;

public class BookingService {
    private Optional<User> user;
    Database dao = new Database();

    public BookingService(Optional<User> user) {
        this.user = user;
    }
   public  void decreaseFreeSeat( int scheduleId, int size){
        Schedule schedule = dao.dbSchedule.get(scheduleId).get();
        schedule.setFreeSeat(schedule.getFreeSeat()-size);
        dao.dbSchedule.delete(scheduleId);
        dao.dbSchedule.create(schedule);
    }
    public  boolean addBooking(int userId, int scheduleId, List<Passenger> passengerList){
        decreaseFreeSeat(scheduleId, passengerList.size());
       return dao.dbBooking.create(new Booking(userId,scheduleId,passengerList));
    }
public   boolean checkFlight(int id){
   return  dao.dbBooking.get(id).isPresent();
}


    public boolean cancelPassenger(int booking_id, int idPassenger) {
          Optional<Booking> booking = dao.dbBooking.get(booking_id);
            List<Passenger> passengers = booking.get().getPassengerList();
            for (Passenger pass: passengers) {
                if (pass.getId()==idPassenger) {
                    passengers.remove(pass);
                    booking.get().setPassengerList(passengers);
                    dao.dbBooking.delete(booking.get().getId());
                    dao.dbBooking.create(booking.get());
                    increaseFreeSeat(booking.get().getScheduleLine(),1);
                    return true;
                }
            }
            return false;
        }

    public boolean deleteFlight(int idNumber){
       List<Booking> all = dao.dbBooking.getAll();
         for (Booking booking: all) {
            if(booking.getUserId() == user.get().getId() && booking.getId()==idNumber){
                increaseFreeSeat(booking.getScheduleLine(), booking.getPassengerList().size());
                return dao.dbBooking.delete(idNumber);
            }

        }
        return false;
    }
    public  void increaseFreeSeat( int scheduleId, int size){
        Schedule schedule = dao.dbSchedule.get(scheduleId).get();
        schedule.setFreeSeat(schedule.getFreeSeat()+size);
        dao.dbSchedule.delete(scheduleId);
        dao.dbSchedule.create(schedule);
    }
public  HashMap<Booking, Schedule> getMyBooking2(){
        HashMap<Booking, Schedule> bookings = new HashMap<>();
    for (Booking booking: dao.dbBooking.getAll()) {
        if(booking.getUserId()==user.get().getId()){
            bookings.put(booking,dao.dbSchedule.get(booking.getScheduleLine()).get() );
        }
    }
    return bookings;
}
}
