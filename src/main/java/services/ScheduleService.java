package services;
import dao.Database;
import entity.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
public class ScheduleService {
    Database dao=new Database();

    public int scheduleCheck(String origin, String destination, String date){
        List<Schedule> all=getAllSchedules();
        int check=-1;
        for(Schedule schedule :all){
            if (schedule.getOriginString().equalsIgnoreCase(origin) &&
                    schedule.getDestinationString().equalsIgnoreCase(destination) &&
                    schedule.parseTime().equals(date)
            ) {
                check = schedule.getId();

            }
        }
        return check;
    }
    public List<Schedule> getAllSchedules(){
        return dao.dbSchedule.getAll();
    }


public  void  generateRandomSchedule(){
     for (int i = 0; i <3; i++) {
        Schedule schedule = new Schedule();
        dao.dbSchedule.create(schedule);
    }

    }
}



