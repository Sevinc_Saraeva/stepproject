package entity;

public enum Airports {
    Arrabury("YARY"),
    Arapoti("HEAR"),
    Alderney("EGJA"),
    Achinsk("UNKS"),
    Barcelos("SWBC"),
    Big_Bell("YBBE"),
    Barcaldine("YBAR"),
    Talhar("OPTH"),
    Broadus("BDX");
    private final String code;

    Airports (String code){
        this.code = code;
    }
}
