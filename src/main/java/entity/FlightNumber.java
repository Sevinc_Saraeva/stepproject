package entity;
import java.io.Serializable;
import java.util.Random;


public class FlightNumber implements Serializable {

private  final Airports FN;
private  final  int number;
    private static long serialVersionUID = 1L;

    public FlightNumber() {
        Random random = new Random();
         this.FN = Airports.values()[random.nextInt(8)];
         number = random.nextInt(100-1)+1;
        }

    @Override
    public String toString() {
        return
                FN +""+number;
    }
}
