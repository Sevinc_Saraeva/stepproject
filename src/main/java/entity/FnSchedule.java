package entity;
import dao.DAO;


public class FnSchedule  {


    public  static int getMaxId(DAO<Schedule> dao){
        return  dao.getAll().stream()
                .map(b -> b.id)
                .max((id1, id2)-> id1 - id2)
                .orElse(0);
    }
}

