package entity;

import dao.DAO;

public class FnUser  {
    private static long serialVersionUID = 1L;

   public  static int getMaxId(DAO<User> dao){
       return  dao.getAll().stream()
               .map(b -> b.getId())
               .max((id1, id2)-> id2 - id1)
               .orElse(0);
   }
   }



