package entity;
import dao.DAO;

public class FnBooking  {
    public static int getMaxId(DAO<Booking> dao){
        return  dao.getAll().stream()
                .map(b -> b.getId())
                .max((id1, id2)-> id2 - id1)
                .orElse(0);
    }
}
