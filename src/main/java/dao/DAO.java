package dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public interface DAO <A>{
    List<A> getAllBy(Predicate<A> p);
    List<A> getAll();
    boolean create(A a) ;
    boolean delete (int id) ;
    Optional<A> get(int id);
    void write(Collection<A> collection);

}
