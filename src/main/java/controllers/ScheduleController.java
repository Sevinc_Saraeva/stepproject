package controllers;

import entity.User;
import entity.Utilities;
import services.BookingService;
import services.ScheduleService;
import java.util.Optional;
import java.util.Scanner;

public class ScheduleController {
    ScheduleService scheduleService=new ScheduleService();

    public void showSchedule(){
        Utilities.showSchedule(scheduleService.getAllSchedules());
    }
    public void showMySchedule(Optional<User> user){
        BookingService bookingService = new BookingService(user);
        Utilities.showMySchedule(bookingService.getMyBooking2());
    }

    public int sizeAllFlight(){
        return  scheduleService.getAllSchedules().size();
    }

    public  void  generateRandomSchedule(){
        scheduleService.generateRandomSchedule();
    }
    public  void showScheduleById(){
        System.out.println("Please, enter the id which you want to see info about: ");
        try {
            int id = new Scanner(System.in).nextInt();
            System.out.println(scheduleService.getAllSchedules().get(id-1));
        } catch (Exception e){
            System.out.println("There is no any flight with this id! ");
        }
    }
}
