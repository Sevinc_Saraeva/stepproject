package controllers;

import entity.*;
import services.RegisterService;
import java.util.*;

public class RegisterController {
    static Scanner in = new Scanner(System.in);

    public  void register(){
        System.out.println("-----Register -----");
        String name = Utilities.getInfo("name");
        String surname =Utilities.getInfo("surname");
        String username =Utilities.getInfo("username");
        Optional <String> password = Optional.empty();
        while((!password.isPresent()) || !ValidationCheck.checkPassword(password.get())){
            System.out.println("Please, enter your password: ");
            System.out.println("Please, keep in mind that:your password must be:  \n 1. greater or equal 8 chars.");
            System.out.println("2. contain at least one lowercase letter.");
            System.out.println("3. contain at least one uppercase letter.");
            System.out.println("4. contain at least one number. ");
            System.out.println("5. contain at least one symbol  like @ , $, & ,*");
            password =Optional.of(in.next());
        }
        final User user = new User(name, surname, username, password.get());
        RegisterService service = new RegisterService();
        service.register(user);
        System.out.println("You are in!");
        MainController mainController = new MainController();
        mainController.run();
    }


}
