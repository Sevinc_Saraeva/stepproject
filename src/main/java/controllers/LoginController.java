package controllers;

import dao.UserDao;
import entity.*;
import services.LoginService;
import java.util.*;

public class LoginController {
    private Optional<User> user;
    UserDao db = new UserDao("user.txt");

    public void login() {
        String username = Utilities.getInfo("username");
        String password = Utilities.getInfo("password");
        LoginService loginService = new LoginService();
        int login = loginService.searchUser(username, password);
        checkLogin(login);
    }

    public Optional<User> getUser() {
        return user;
    }

    public void setUser(Optional<User> user) {
        this.user = user;
    }

    public void checkLogin(int login){
        if (login != -1) {
            this.user = db.get(login);
            System.out.println("Welcome to our app!!");
            LoginMain loginMain = new LoginMain(user);
            loginMain.loginMenu();
        } else {
            System.out.println("Username or password is incorrected, Please, try again");
            MainController mainController = new MainController();
            mainController.run();
        }
    }
}