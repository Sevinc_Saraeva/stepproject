package controllers;
import entity.Print;
import entity.User;

import java.util.Optional;
import java.util.Scanner;

public class MainController {
    Scanner in = new Scanner(System.in);
    RegisterController registerController = new RegisterController();
    LoginController loginController = new LoginController();
    ScheduleController scheduleController =new ScheduleController();
    Print print = new Print();



    public void run(){
        generateRandomSchedule();
        print.printMain();
        boolean k = false;
        while (!k){
            String input = in.next();
            switch (input){
                case "1":
                    k = true;
                    register();
                    break;
                case  "2" :
                    k = true;
                    login();
                    break;
                case  "3":
                    k = true;
                    showSchedule();
                    break;
                case  "4":
                    k = true;
                    showScheduleById();
                    break;
                case  "0":
                     print.Goodbye();
                      break;
                default:
                    k = false;
                    System.out.println("Please try again!!");
                    break;
            }
        }
    }

    public  void register(){
       registerController.register();
    }

    public void login(){
     loginController.login();
    }

    public void showSchedule(){
        scheduleController.showSchedule();
        run();
    }
    public  void showScheduleById(){
        scheduleController.showScheduleById();
        run();
    }
    public  void generateRandomSchedule(){
        scheduleController.generateRandomSchedule();
    }
}
